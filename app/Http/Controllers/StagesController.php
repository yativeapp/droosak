<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Stages;

class StagesController extends Controller
{


    public function index()
    {
        $select = intVal(request('depdrop_parents')[0]);

        return $this->buildSelect(config('stages.stage-' . $select));
    }


    public function getSchools(Stages $stage)
    {

        $class = $stage->getUserClass(request('stage1'));

        return $class::where('name', 'like', '%' . request('q') . '%')
            ->where('stage1', request('stage1'))
            ->where('stage2', request('stage2'))->get();

    }


    private function buildSelect($data)
    {

        $output = [];

        foreach ($data as $key => $select) {
            $output[] = [
                'id' => (string)$key,
                'name' => $select
            ];
        }

        return [
            'output' => $output,
            'selected' => 0
        ];
    }
}
