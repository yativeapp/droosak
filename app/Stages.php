<?php namespace App;


use App\Models\PrimarySchools;
use App\Models\SecondarySchools;
use App\Models\universties;

class Stages
{
     public function getUserClass($stage)
     {
          $stage1 = intVal($stage);

          $class = null;

          switch ($stage1) {
               case 0:
                    $class = new PrimarySchools;
                    break;
               case 1:
                    $class = new SecondarySchools;
                    break;
               case 2:
                    $class = new universties;
                    break;
          }

          return $class;
     }

     public function createSchool($stage1, $stage2, $name)
     {

          $school = $this->getUserClass($stage1)->firstOrcreate(compact('stage1', 'stage2', 'name'));

          return $school->group()->firstOrcreate([]);
     }
}