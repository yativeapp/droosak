<?php

/**
 * Created by lvntayn
 * Date: 03/06/2017
 * Time: 22:45
 */

namespace App\Models;


use DB;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $table = 'groups';

    public $timestamps = false;

    public function school()
    {
        return $this->morphTo();
    }


    public function countPeople($id = 0, $country = false)
    {

        return User::where('group_id', $id)->count();

    }

    public function countAllCountries()
    {

        $s = Group::leftJoin('user_hobbies', 'user_hobbies.hobby_id', '=', 'groups.hobby_id')
            ->leftJoin('user_locations', 'user_locations.user_id', '=', 'user_hobbies.user_id')
            ->leftJoin('cities', 'cities.id', '=', 'user_locations.city_id')
            ->leftJoin('countries', 'countries.id', '=', 'cities.country_id')
            ->where('groups.hobby_id', $this->hobby_id)
            ->select(DB::raw('count(*) as count, countries.*'))->groupBy('countries.id')->get();

        return $s;
    }
}