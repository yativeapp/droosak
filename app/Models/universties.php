<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group;

class universties extends Model
{

    protected $fillable = ['stage1', 'stage2', 'name'];



    public function group()
    {

        return $this->morphOne(Group::class, 'school');
    }
}
