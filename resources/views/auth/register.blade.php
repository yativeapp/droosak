
<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.6.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
          <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
        <link rel="manifest" href="/icons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
  <meta name="description" content="Web Creator Description">
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  <link rel="stylesheet" href="css/dependent-dropdown.min.css" type="text/css">


      <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - signup</title>
  
  
   <style>
       body{
           direction:rtl;
       }
     .mbr-overlay{
         opacity:1;
background: #F7971E;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #FFD200, #F7971E);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #FFD200, #F7971E); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

     }

     /* #stage-2 , #stage-3 { display :none;} */
   </style>
</head>
<body>
  <section class="cid-qMXeN3GGYA mbr-fullscreen" id="header15-1p">

    

    <div class="mbr-overlay"></div>

    <div class="container align-center">
<div class="row justify-content-md-center">
    <div class="col-lg-4 col-md-5">
        <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">دروسك</h1>
    <div class="form-container">
        <div class="media-container-column">
            <form class="mbr-form" action="{{ route('register') }}" method="post">

            {{ csrf_field() }}
                <div data-for="name">
                                   @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
                    <div class="form-group">
                        <input type="text" class="form-control px-3" name="name"  placeholder="ألاسم"   value="{{ old('name') }}">
                    </div>
                </div>
                <div data-for="email">
                                    @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
                    <div class="form-group">
                        <input type="email" class="form-control px-3" name="email"  placeholder="البريد ألالكترونى"   value="{{ old('email') }}">
                    </div>

                </div>
                                <div data-for="password">
                                    @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                    <div class="form-group">
                        <input type="password" class="form-control px-3" name="password"  placeholder="الرقم السرى">
                    </div>

                </div>
                <div data-for="phone">
                                     @if ($errors->has('phone'))
                    <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
                    <div class="form-group">
                        <input type="tel" class="form-control px-3" name="phone"  placeholder="رقم الهاتف" value="{{ old('phone') }}">
                    </div>

                </div>
                <div data-for="sex">
                                     @if ($errors->has('sex'))
                    <span class="help-block">
                    <strong>{{ $errors->first('sex') }}</strong>
                </span>
                @endif
                    <div class="form-group">
                        <select class="form-control" name="sex">
                            <option value="0">ذكر</option>
                            <option value="1">أنثى</option>
                        </select>
                    </div>

                </div>
                    <div class="form-group">
                    <!-- HTML Markup (Parent) -->
                        <select id="stage-1" class="form-control" name="stage1">
                            <option id="">مرحلتك التعليمية</option>

                            @foreach(config('stages.all') as $index => $stage)
                              <option id="{{ $index }}" value="{{ $index }}">{{ $stage }}</option>
                            @endforeach
                            <!-- other options -->
                        </select>

                    </div>
                    <div class="form-group">
                    <!-- HTML Markup (Parent) -->
                        <select id="stage-2" class="form-control" name="stage2">
                            <option id="">الصف</option>
                            <!-- other options -->
                        </select>

                    </div>
                    <div class="form-group">
                     <input name="schoolname" type="text" class="form-control px-3 typeahead" placeholder="أسم مدرستك أو جامعتك" autocomplete="off">
                    </div>

                
                <span class="input-group-btn">
                    <button href="" type="submit" class="btn btn-secondary btn-form display-4">تسجيل</button>
                </span>
            </form>
        </div>
    </div>
    </div>
</div>
    </div>
    
</section>

<script src="{{ asset('plugins/jquery/jquery-2.1.4.min.js')  }}"></script>

<script src="js/dependent-dropdown.min.js"></script>
<script src="js/bootstrap-typeahead.min.js"></script>

<!-- Javascript call on document ready -->
<script>

$(document).ready(function(){
        $('input.typeahead').typeahead({
            ajax: {
                url : '/stages/get',
                preDispatch : function(q){
                    var stage1 = $('#stage-1').val();
                    var stage2 = $('#stage-2').val();
                     return { query: q , stage1 : stage1 , stage2 : stage2 }
                }
            }
        });

    $("#stage-2").depdrop({
        url: '/stages/get',
        depends: ['stage-1']
    });

})

 

</script>
  </body>
</html>