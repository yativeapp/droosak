<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.6.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
          <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
        <link rel="manifest" href="/icons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
  <meta name="description" content="Web Page Maker Description">
  <title>droosak</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons-bold/mobirise-icons-bold.css">
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="cid-qMWrWJNz8B mbr-fullscreen" data-bg-video="https://player.vimeo.com/video/205512857?title=0&amp;portrait=0&amp;byline=0&amp;autoplay=1" id="header2-19">

    

    <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(35, 35, 35);"></div>

    <div class="container align-center">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">دروسك</h1>
                
                <p class="mbr-text pb-3 mbr-fonts-style display-5">دروسك هو اول موقع تعليمى شامل من نوعة فى مصر يحققلك النجاح بأقل المصاريف وفي دروسك الطالب هيلاقي كل الخدمات التعليمية في مكان واحد وبشكل يسهل عليه إنه يذاكر ويضمن نجاحه بمعنى انك تقدر تحضر دروس مباشرة مع مدرس حقيقى اوتشوفها مسجله بكامل احدثها او تتعلمها بنفسك من خلال ملزمة دروسك التفاعلية ودروسك بيستخدم أحدث أساليب التكنولوجيا في الشرح والمتابعة والتقييم بطريقة ممتعة، ببساطة مع دروسك هتوفر وقت ومجهود وفلوس لان كل احتياجاتك موجودة على دروسك اونلاين</p>
                <div class="mbr-section-btn"><a class="btn btn-md btn-secondary display-4" href="{{ route('register') }}">تسجيل</a>
                    <a class="btn btn-md btn-white-outline display-4" href="{{ route('login') }}">دخول</a></div>
            </div>
        </div>
    </div>
    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="features1 cid-qMWTDM9d27" id="features1-1g">
    
    

    
    <div class="container">
        <div class="media-container-row">

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-cash"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">وفر أموالك</h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                        دروسك يوفرلك أموالك عن طريق شرح الدروس لمرحلتك العلمية على النترنت بدلا من الدروس الخصوصية
                    </p>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-edit2"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">تعلم</h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                    دروسك يوفرلك طريقة تفاعلية و جديدة لشرح الدروس
                    </p>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="mbr-iconfont mbri-help"></span>
                </div>
                <div class="card-box">
                    <h4 class="card-title py-3 mbr-fonts-style display-5">تواصل</h4>
                    <p class="mbr-text mbr-fonts-style display-7">
                    دروسك يمكنك من التواصل مع المحيطين بك و زملائك فى مدرستك أو جامعتك
                    </p>
                </div>
            </div>

            

        </div>

    </div>

</section>

<section class="mbr-section content5 cid-qMWU6T1bzg mbr-parallax-background" id="content5-1h">

    

    <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">الحل ألامثل لطريقك التعليمى</h2>
            </div>
        </div>
    </div>
</section>

<section class="features12 cid-qMWWaU9BEc" id="features12-1j">
    
    

    

    <div class="container">
        
        

        <div class="media-container-row pt-5">
            <div class="block-content align-right">
                <div class="card pl-3 pr-3 pb-5">
                    <div class="mbr-card-img-title">
                        <div class="card-img pb-3">
                             <span class="mbr-iconfont mbrib-cust-feedback"></span>
                        </div>
                        <div class="mbr-crt-title">
                            
                        </div>
                    </div>                

                    <div class="card-box">
                        <p class="mbr-text mbr-section-text mbr-fonts-style display-7">يمكنك التواصل مع جميع الطلاب فى مرحلتك العلمية&nbsp;</p>
                    </div>
                </div>

                <div class="card pl-3 pr-3 pb-5">
                    <div class="mbr-card-img-title">
                        <div class="card-img pb-3">
                            <span class="mbr-iconfont mbrib-target"></span>
                        </div>
                        <div class="mbr-crt-title">
                            
                        </div>
                    </div>
                    <div class="card-box">
                        <p class="mbr-text mbr-section-text mbr-fonts-style display-7">تواصل مع جميع الطلبة بالقرب منك.
                        </p>
                    </div>
                </div>
            </div>

            <div class="mbr-figure" style="width: 50%;">
                <img src="assets/images/mbr-982x541.jpg" alt="Mobirise" title="">
            </div>

            <div class="block-content align-left  ">
                <div class="card pl-3 pr-3 pb-5">
                    <div class="mbr-card-img-title">
                        <div class="card-img pb-3">
                             <span class="mbr-iconfont mbrib-bookmark"></span>
                        </div>
                        <div class="mbr-crt-title">
                            
                        </div>
                    </div>                

                    <div class="card-box">
                        <p class="mbr-text mbr-section-text mbr-fonts-style display-7">دروسك يتضمن جميع المراحل التعليمية فى مكان ولحد</p>
                    </div>
                </div>

                <div class="card pl-3 pr-3 pb-5">
                    <div class="mbr-card-img-title">
                        <div class="card-img pb-3">
                            <span class="mbr-iconfont mbrib-cash"></span>
                        </div>
                        <div class="mbr-crt-title">
                            
                        </div>
                    </div>
                    <div class="card-box">
                        <p class="mbr-text mbr-section-text mbr-fonts-style display-7">أدفع أقل بكثير مما تدفعه فى الدروس الخصوصية</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="counters1 counters cid-qMWXPxAN1j mbr-parallax-background" id="counters1-1k">

    

    <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container">
        <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2" style="color:white;"><strong>مستقبل الدراسة عبر الانترنت</strong></h2>
        <h3 class="mbr-section-subtitle mbr-fonts-style display-5">سجل الأن و كن على تواصل مع</h3>

        <div class="container pt-4 mt-2">
            <div class="media-container-row">
                <div class="card p-3 align-center col-12 col-md-6">
                    <div class="panel-item p-3">
                        

                        <div class="card-text">
                            <h3 class="count pt-3 pb-3 mbr-fonts-style display-2">
                                  500
                            </h3>
                            <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">
                                طالب</h4>
                            <p class="mbr-content-text mbr-fonts-style display-7">وجدوا دروسك الحل المثالى للتعليم عبر الانترنت و يمكنك مشاركتهم الان</p>
                        </div>
                    </div>
                </div>


                <div class="card p-3 align-center col-12 col-md-6">
                    <div class="panel-item p-3">
                        
                        <div class="card-text">
                            <h3 class="count pt-3 pb-3 mbr-fonts-style display-2">
                                  200
                            </h3>
                            <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">مدرس</h4>
                            <p class="mbr-content-text mbr-fonts-style display-7">من أفضل المدرسين على مستوى مصر</p>
                        </div>
                    </div>
                </div>

                


                
            </div>
        </div>
   </div>
</section>

<section class="mbr-section form3 cid-qMX0gxheKt" id="form3-1m">

    

    

    <div class="container">
        <div class="row justify-content-center">
            <div class="title col-12 col-lg-8">
                <h2 class="align-center pb-2 mbr-fonts-style display-2">أشترك فى نشرتنا البريدية</h2>
                <h3 class="mbr-section-subtitle align-center pb-5 mbr-light mbr-fonts-style display-5">كن على تواصل بكل جديد فى دروسك</h3>
            </div>
        </div>

        <div class="row py-2 justify-content-center">
            <div class="col-12 col-lg-6  col-md-8 " data-form-type="formoid">
                <div data-form-alert="" hidden="">
                        Thanks for filling out the form!
                </div>
                <form class="mbr-form" action="https://mobirise.com/" method="post" data-form-title="Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="Lg5bWvNnqH10uscW4ULSlrBZbfm2N3Tzgzbr+BX6yG/OihVX4fuddi87xXyOP6MPIInDQA73N6WJSYvOj7AcIugmBWFUVjUyltQQINpwpkYO8h20o6aUtDe/p1TTMLVx" data-form-field="Email">
                    <div class="mbr-subscribe input-group">
                        <input class="form-control" type="email" name="email" placeholder="Email" data-form-field="Email" required="" id="email-form3-1m">
                        <span class="input-group-btn"><button href="" type="submit" class="btn btn-primary display-4">أشترك</button></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="cid-qMX0niUA3I" id="footer2-1n">

    

    

    <div class="container">
        <div class="media-container-row content mbr-white">
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <p class="mbr-text">
                    <strong>Address</strong>
                    <br>
                    <br>1234 Street Name
                    <br>City, AA 99999
                    <br>
                    <br>
                    <br><strong>Contacts</strong>
                    <br>
                    <br>Email: support@mobirise.com
                    <br>Phone: +1 (0) 000 0000 001
                    <br>Fax: +1 (0) 000 0000 002
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <p class="mbr-text">
                    <strong>Links</strong>
                    <br>
                    <br><a class="text-primary" href="https://mobirise.com/">Website builder</a>
                    <br><a class="text-primary" href="https://mobirise.com/mobirise-free-win.zip">Download for Windows</a>
                    <br><a class="text-primary" href="https: //mobirise.com/mobirise-free-mac.zip">Download for Mac</a>
                    <br>
                    <br><strong>Feedback</strong>
                    <br>
                    <br>Please send us your ideas, bug reports, suggestions! Any feedback would be appreciated.
                </p>
            </div>
            <div class="col-12 col-md-6">
                <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJn6wOs6lZwokRLKy1iqRcoKw" allowfullscreen=""></iframe></div>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2017 Mobirise - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.youtube.com/c/mobirise" target="_blank">
                                <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://instagram.com/mobirise" target="_blank">
                                <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://plus.google.com/u/0/+Mobirise" target="_blank">
                                <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.behance.net/Mobirise" target="_blank">
                                <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="assets/parallax/jarallax.min.js"></script>
  <script src="assets/viewportchecker/jquery.viewportchecker.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/formoid/formoid.min.js"></script>
  
  
 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i></i></a></div>
  </body>
</html>